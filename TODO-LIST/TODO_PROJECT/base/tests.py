from django.core import mail
from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User
from django.utils import timezone
from datetime import timedelta, datetime
from base.models import Todo
from .views import completetodo

class RegisterViewTest(TestCase):
    def test_register_view(self):
        # Create a test user
        user_data = {
            'username': 'testuser',
            'password1': 'testpassword',
            'password2': 'testpassword',
            'email': 'test@example.com',
            'first_name': 'Test',
            'last_name': 'User',
        }
        
        response = self.client.post(reverse('register'), data=user_data)
        
        # Check if the user is created
        self.assertEqual(response.status_code, 302)  # Check for a redirect status code
        self.assertEqual(User.objects.count(), 1)
        user = User.objects.get(username='testuser')
        self.assertEqual(user.email, 'test@example.com')
        
        # Cleanup - Delete the created user
        user.delete()


class UserLoginTest(TestCase):
    def setUp(self):
        # Create a user for testing
        self.username = 'testuser'
        self.password = 'testpassword'
        self.user = User.objects.create_user(username=self.username, password=self.password)
    
    def test_user_login(self):
        # Simulate user login
        response = self.client.post(reverse('loginuser'), {'username': self.username, 'password': self.password})
        
        # Check if the user is logged in and redirected to the currenttodos page
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('currenttodos'))
        self.assertIn('_auth_user_id', self.client.session)



class TodoDeleteTestCase(TestCase):
    def setUp(self):
        # Create a user for authentication
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        
        # Create a todo object associated with the user
        self.todo = Todo.objects.create(title='Test Todo', user=self.user)
        
    def test_delete_todo(self):
        # Log in the user
        self.client.login(username='testuser', password='testpassword')
        
        # Get the delete URL for the todo
        delete_url = reverse('deletetodo', args=[self.todo.pk])
        
        # Send a POST request to delete the todo
        response = self.client.post(delete_url)
        
        # Assert that the todo was deleted successfully
        self.assertEqual(response.status_code, 302)
        self.assertFalse(Todo.objects.filter(pk=self.todo.pk).exists())
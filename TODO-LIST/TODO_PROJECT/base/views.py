from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from .forms import TodoForm
from .models import Todo
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django import forms
import json
from django.core import serializers
from datetime import date
from django.http import HttpResponseNotAllowed
from datetime import timedelta
from django.views.decorators.http import require_http_methods
import datetime

#sending confirmation email
from django.conf import settings
from django.core.mail import send_mail
from django.views.decorators.csrf import csrf_protect

def home(request):
    return render(request, 'base/home.html')

class ExtendedUserCreationForm(UserCreationForm):
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=30)
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].help_text = ''
        self.fields['password1'].help_text = ''
        self.fields['password2'].help_text = ''

@csrf_protect 
def register(request):
    if request.method == 'POST':
        form = ExtendedUserCreationForm(request.POST)
        if form.is_valid() and form.cleaned_data['password1'] == form.cleaned_data['password2']:
            try:
                user = form.save(commit=False)
                user.set_password(form.cleaned_data['password1'])
                user.save()
                # Sending email
                subject = 'Welcome to THE TODO APP'
                message = f'Hi {user.username}, thank you for registering in the TODO APP.'
                email_from = settings.EMAIL_HOST_USER
                recipient_list = [user.email]
                send_mail(subject, message, email_from, recipient_list)
                login(request, user)
                return redirect('loginuser')
            except IntegrityError:
                return render(request, 'base/register.html', {'form': ExtendedUserCreationForm(), 'error': 'That username has already been taken. Please choose a new username'})
        else:
            return render(request, 'base/register.html', {'form': form, 'error': 'Invalid form data or passwords did not match'})
    else:
        form = ExtendedUserCreationForm()
        return render(request, 'base/register.html', {'form': form, 'user': request.user})


def loginuser(request):
    if request.method == 'GET':
        return render(request, 'base/loginuser.html', {'form':AuthenticationForm()})
    else:
        user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
        if user is None:
            return render(request, 'base/loginuser.html', {'form':AuthenticationForm(), 'error':'Username and password did not match'})
        else:
            login(request, user)
            return redirect('currenttodos')

@login_required
def logoutuser(request):
    if request.method == 'POST':
        logout(request)
        return redirect('home')

@login_required
def createtodo(request):
    if request.method == 'GET':
        return render(request, 'base/createtodo.html', {'form':TodoForm()})
    else:
        try:
            form = TodoForm(request.POST)
            newtodo = form.save(commit=False)
            newtodo.user = request.user
            newtodo.save()
            return redirect('currenttodos')
        except ValueError:
            return render(request, 'base/createtodo.html', {'form':TodoForm(), 'error':'Bad data passed in. Try again.'})

@login_required
def currenttodos(request):
    todos = Todo.objects.filter(user=request.user, datecompleted__isnull=True)

    # Update todos with due_date in the past as "skipped"
    now = timezone.now().date()
    for todo in todos:
        if todo.due_date < now:
            todo.status = "skipped"
            todo.save()

    return render(request, 'base/currenttodos.html', {'todos': todos})


@login_required
def completedtodos(request):
    todos = Todo.objects.filter(user=request.user, datecompleted__isnull=False).order_by('-datecompleted')
    return render(request, 'base/completedtodos.html', {'todos':todos})

@login_required
def viewtodo(request, todo_pk):
    todo = get_object_or_404(Todo, pk=todo_pk, user=request.user)
    if request.method == 'GET':
        form = TodoForm(instance=todo)
        return render(request, 'base/viewtodo.html', {'todo':todo, 'form':form})
    else:
        try:
            form = TodoForm(request.POST, instance=todo)
            form.save()
            return redirect('currenttodos')
        except ValueError:
            return render(request, 'base/viewtodo.html', {'todo':todo, 'form':form, 'error':'Bad info'})

@login_required
def completetodo(request, todo_pk):
    todo = get_object_or_404(Todo, pk=todo_pk, user=request.user)
    if request.method == 'POST':
        todo.datecompleted = timezone.now()
        todo.save()

        # Send reminder email
        if todo.due_time:
            reminder_time = todo.due_time - timedelta(hours=1)
            if timezone.now() >= reminder_time:
                subject = 'Reminder: Your task is due soon'
                message = f"Your task '{todo.title}' is due in 1 hour. Please complete it on time."
                from_email = settings.EMAIL_HOST_USER
                to_email = [request.user.email]
                send_mail(subject, message, from_email, to_email)

        return redirect('currenttodos')

@login_required
def deletetodo(request, todo_pk):
    todo = get_object_or_404(Todo, pk=todo_pk, user=request.user)
    if request.method == 'POST':
        todo.delete()
        return redirect('currenttodos')



from django import template

register = template.Library()

@register.filter
def is_datetime(value):
    return isinstance(value, (datetime.datetime, datetime.date))

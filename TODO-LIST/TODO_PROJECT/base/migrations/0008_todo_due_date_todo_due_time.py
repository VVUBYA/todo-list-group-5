# Generated by Django 4.2 on 2023-05-27 19:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0007_remove_task_due_date_task_duedate'),
    ]

    operations = [
        migrations.AddField(
            model_name='todo',
            name='due_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='todo',
            name='due_time',
            field=models.TimeField(blank=True, null=True),
        ),
    ]

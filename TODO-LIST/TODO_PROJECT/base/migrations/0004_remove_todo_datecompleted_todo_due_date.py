# Generated by Django 4.2 on 2023-05-25 07:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0003_todo'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='todo',
            name='datecompleted',
        ),
        migrations.AddField(
            model_name='todo',
            name='Due_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]

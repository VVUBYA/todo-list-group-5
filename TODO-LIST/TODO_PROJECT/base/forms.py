from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.db import models
from datetime import date
from django.forms import ModelForm
from .models import Todo

# Sign Up Form
class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional')
    email = forms.EmailField(max_length=50,)

    class Meta:
        model = User
        fields = [
            'username', 
            'first_name', 
            'last_name', 
            'email', 
            'password1', 
            'password2', 
            ]


class TodoForm(ModelForm):
    due_date = forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}))
    due_time = forms.DateTimeField( widget=forms.DateTimeInput(attrs={'type': 'time'}), input_formats=['%H:%M']  )

    
    class Meta:
        model = Todo
        fields = ['title', 'memo', 'important', 'due_date', 'due_time']


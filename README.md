Functional Specification

The to-do app is a web-based application that allows users to create and manage their tasks. It provides the following functionalities:

User registration and login: Users can create an account using their email address and password. After logging in, they can view, edit, and delete their tasks.

Task creation: Users can create a new task by specifying the task name, expected date and time, and reminder time (in minutes).

Task editing: Users can edit their tasks by changing the task name, description, expected date and time, or reminder time.

Task completion: Users can mark a task as completed when they finish it. The app will update the task status and remove the reminder.

Task deletion: Users can delete a task if they no longer need to complete it.

Task reminder: The app will send an email reminder to the user at the specified reminder time. The email will contain a link to the task page where the user can view and update the task.

Task status: The app will automatically mark a task as skipped if its expected date and time have passed and the task is still incomplete.

Task history: The app will store completed and skipped tasks for seven days. After seven days, the app will remove the tasks from the database.


Technological Specification for a To-Do App:

Frontend: The frontend of the app should be developed using a modern JavaScript framework such as Angular with HTML, CSS for the user interface.

Backend: The backend of the app should be developed using a Django framework plus python and REST API.

Database: The app should use a relational database such as MySQL to store user information and task data.

Email Service: The app should use an email service such as SendGrid or Mailgun to send reminder emails to users.

Deployment: The app should be deployed to a cloud service such as Heroku or AWS.